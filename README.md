# README #

This application adds the following hotkeys for media keys:

* Start/Pause (Ctrl + F5)
* Stop (Ctrl + F6) 
* Previous Track (Ctrl + F7)
* Next Track (Ctrl + F8)

### How do I get set up? ###

* Clone Repository
* Open the MediaKeys.sln in and compile
* Dependencies are managed via nuget. All dependencies are included and it builds out of the box

### Deployment instructions ###
* Add a shortcut to the application to the startup folder (e.g. press `windows + r` and type `shell:startup` to find that folder in Windows 8)
* or start the application manually if you want the hotkeys enabled.
* To "uninstall" just stop the application (e.g. via `ctrl + alt + delete` TaskManager) and delete the files.

### Contribution guidelines ###

Got suggestions? Submit a pull request or create your own fork.

### Acknowledgments ###
Icon created by [Lorc](http://lorcblog.blogspot.com/)under [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/). Resized by me.

HotkeyManager Copied from [Chris Taylor](http://stackoverflow.com/a/3654821/27083). Rearranged by me.

[Inputsimulator](http://inputsimulator.codeplex.com) by Michael Noonan under [Ms-PL](http://inputsimulator.codeplex.com/license).