﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsInput.Native;

/// Favicon http://game-icons.net/lorc/originals/bugle-call.html created by Lorc under CC BY 3.0. Resized by me.

namespace MediaKeys
{
    class Program
    {
        static void Main(string[] args)
        {
            var preventClosing = new ManualResetEvent(false);
            
            Application.ApplicationExit += (sender, eventArgs) => { preventClosing.Set(); };

            var hotkeyToVirtualkeyMap = new Dictionary<Keys, VirtualKeyCode>() { 
                                        {Keys.F5, VirtualKeyCode.MEDIA_PLAY_PAUSE}, 
                                        {Keys.F6, VirtualKeyCode.MEDIA_STOP}, 
                                        {Keys.F7, VirtualKeyCode.MEDIA_PREV_TRACK}, 
                                        {Keys.F8, VirtualKeyCode.MEDIA_NEXT_TRACK} };

            WindowsInput.InputSimulator keystrokesimulator = new WindowsInput.InputSimulator();
            foreach (var hotkey in hotkeyToVirtualkeyMap.Keys)
            {
                HotKeyManager.RegisterHotKey(hotkey, KeyModifiers.Control);
            }

            HotKeyManager.HotKeyPressed += (s, o) => { keystrokesimulator.Keyboard.KeyPress(hotkeyToVirtualkeyMap[o.Key]); };

            preventClosing.WaitOne();
        }
        
    }
}
